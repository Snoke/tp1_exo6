from Calculator.operations import *
import unittest

operation = Operation()

class OperatorTestCase(unittest.TestCase):

	test_values_addition = ((5, 4, 9), (10, 2, 12), (18, 52, 70), (23, 6, 29))
	test_values_soustraction = ((18, 2, 16), (10, 15, -5), (25, 25, 0))
	test_values_multiplication = ((2, 5, 10), (12, 12, 144), (5, -5, -25))
	test_values_division = ((25, 5, 5), (-25, 5, -5), (26, 26, 1))

	def test_addition(self):
		for val1, val2, expected in self.test_values_addition:
			result = operation.addition(val1, val2)
			print(result)
			self.assertEqual(expected, result)
	def test_soustraction(self):
		for val1, val2, expected in self.test_values_soustraction:
			result = operation.soustraction(val1, val2)
			print(result)
			self.assertEqual(expected, result)
	def test_multiplication(self):
		for val1, val2, expected in self.test_values_multiplication:
			result = operation.multiplication(val1, val2)
			print(result)
			self.assertEqual(expected, result)
	def test_division(self):
		for val1, val2, expected in self.test_values_division:
			result =  operation.division(val1, val2)
			print(result)
			self.assertEqual(expected, result)

if __name__ == '__main__':
	unittest.main()
	Test = OperatorTestCase()
	Test.test_addition()
	Test.test_soustraction()
	Test.test_division()
	Test.test_multiplication()